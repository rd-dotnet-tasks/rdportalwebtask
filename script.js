function validate() {
  if (
    !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(regform.email.value)
  ) {
    alert("You have entered an invalid email address");
    return false;
  }
  if (!/^[a-zA-Z]+$/.test(regform.firstname.value)) {
    alert("You have entered an invalid first name");
    return false;
  }
  if (!/^[a-zA-Z]+$/.test(regform.lastname.value)) {
    alert("You have entered an invalid last name");
    return false;
  }
  if (!/^[789]\d{9}$/.test(regform.contact.value)) {
    alert("You have entered an invalid contact number");
    return false;
  }
  alert("Registration successful");
  return true;
}
